using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

//
public class DialogueSystem : MonoBehaviour
{
    [SerializeField] private NarasiTeks[] dialogue;

    [SerializeField] private int index;
    
    
    [SerializeField] Button Next_BTN,Skip_BTN,exitButton;
    [SerializeField] Text dialogueText;
    
    
    [SerializeField] SaveAndLoad saveAndLoad;


    private string sceneName;
    public bool startScene;
    public bool endScene;
    public List<IsiData> datalist = new List<IsiData>();
    public string fileName = "Narasi";

    [SerializeField] Image icon;

    GameObject Additional;
    private int ModeGamePlay;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        ModeGamePlay = PlayerPrefs.GetInt("ModeGamePlay");
        if (ModeGamePlay == 0)
        {
            gameObject.SetActive(false);
            yield return null;
        }
        sceneName = SceneManager.GetActiveScene().name;
        Next_BTN.onClick.AddListener(NextDialogue);
        Skip_BTN.onClick.AddListener(SkipDialogue);
        exitButton.onClick.AddListener(Exit);

        if (LanguageManager.instance.GetLanguageIndex() == 0)
        {
            dialogueText.text = dialogue[index].narasiTeks;
        }
        else if (LanguageManager.instance.GetLanguageIndex() == 1)
        {
            dialogueText.text = dialogue[index].terjemahanTeks;
        }
        icon.sprite = dialogue[index].icon.sprite;
        
        if (endScene)
        {
            Additional = transform.Find("Canvas/FinishPanel").gameObject;
        }
        LoadData();
    }


    private void Exit()
    {
        SceneManager.LoadScene("Game");
    }

    void NextDialogue()
    {
        index++;
        if (index < dialogue.Length)
        {
            if (LanguageManager.instance.GetLanguageIndex() == 0)
            {
                dialogueText.text = dialogue[index].narasiTeks;
            }
            else if (LanguageManager.instance.GetLanguageIndex() == 1)
            {
                dialogueText.text = dialogue[index].terjemahanTeks;
            }
            icon.sprite = dialogue[index].icon.sprite;
        }
        else
        {
            if(endScene)
            {
                Save();
                Additional.SetActive(true);
            }
            if(startScene)
            {
                Save();
                FindObjectOfType<GameManager>().MunculSpawner();
                gameObject.SetActive(false);

            }
        }
    }

    void SkipDialogue()
    {
        if (endScene)
        {
            Save();
            Additional.SetActive(true);
        }
        if (startScene)
        {
            Save();
            FindObjectOfType<GameManager>().MunculSpawner();
            gameObject.SetActive(false);

        }
    }
    private void Save()
    {
        if(datalist == null)
        {
            IsiData temp = new IsiData { sceneName = sceneName, startScene = false, endScene = false };
            datalist.Add(temp);
            saveAndLoad.SaveToJSON<IsiData>(datalist, fileName);
        }
        else
        {
            string a = CheckSceneName();
            if(a == null)
            {
                IsiData temp = new IsiData { sceneName = sceneName, startScene = false, endScene = false };
                datalist.Add(temp);
                saveAndLoad.SaveToJSON<IsiData>(datalist, fileName);
            }
            else
            {
                for (int i = 0; i < datalist.Count; i++)
                {
                    if(datalist[i].sceneName == a)
                    {
                        if(startScene)
                        {
                            datalist[i].startScene = true;
                        }
                        if (endScene)
                        {
                            datalist[i].endScene = true;
                        }
                        saveAndLoad.SaveToJSON<IsiData>(datalist, fileName);
                    }
                }
            }
        }
    }
    public string CheckSceneName()
    {
        for (int i = 0; i < datalist.Count; i++)
        {
            if (datalist[i].sceneName == sceneName)
                return datalist[i].sceneName;
        }
        return null;
    }
    private void LoadData()
    {
        List<IsiData> datalistTemp = saveAndLoad.ReadListFromJSON<IsiData>(fileName);
        if (datalistTemp == null)
        {
            IsiData newdata = new IsiData { sceneName = sceneName, startScene = false, endScene = false };
            datalist.Add(newdata);
            saveAndLoad.SaveToJSON<IsiData>(datalist, "Narasi");
            transform.Find("Canvas/TextBoxBTN").gameObject.SetActive(true);
        }
        else
        {
            datalist = datalistTemp;
            string a = CheckSceneName();
            if(a == null)
            {
                IsiData newdata = new IsiData { sceneName = sceneName, startScene = false, endScene = false };
                datalist.Add(newdata);
                saveAndLoad.SaveToJSON<IsiData>(datalist, "Narasi");
                transform.Find("Canvas/TextBoxBTN").gameObject.SetActive(true);
            }
            else
            {
                for (int i = 0; i < datalist.Count; i++)
                {
                    if(datalist[i].sceneName == a)
                    {
                        if (startScene && datalist[i].startScene)
                        {
                            FindObjectOfType<GameManager>().MunculSpawner();
                            gameObject.SetActive(false);
                        }
                        else if(startScene &&!datalist[i].startScene)
                        {
                            transform.Find("Canvas/TextBoxBTN").gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }
}

[System.Serializable]
public class NarasiTeks
{
    [TextArea(1,5)]
    public string narasiTeks;
    [TextArea(1, 5)]
    public string terjemahanTeks;

    public SpriteRenderer icon;
}