using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HourlyReward : MonoBehaviour
{
    public float currentSeconds = 7200;
    public STATE state;

    public Sprite[] icons;

    public Button chestBTN;

    public Text rewardTeks;
    public Text timerTeks;

    public GameObject dailySpin;

    public string serverAddress;

    public DateTime currentTime;
    public DateTime claimTime;
    private void Start()
    {
        chestBTN.onClick.AddListener(Claim);
        state = STATE.WAITING;
        StartCoroutine(GetCurrentTime());
    }

    private IEnumerator GetCurrentTime()
    {
        UnityWebRequest request = UnityWebRequest.Get(serverAddress);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.Success:
                string data = request.downloadHandler.text;
                currentTime = DateTime.Parse(ReturnStringFromWorldTimeApi(data));
                if (PlayerPrefs.HasKey("HourlyReward"))
                {
                    long temp = Convert.ToInt64(PlayerPrefs.GetString("HourlyReward"));
                    claimTime = DateTime.FromBinary(temp);
                    if (currentTime>claimTime)
                    {
                        state = STATE.CLAIMAVAILABLE;
                        chestBTN.GetComponent<Image>().sprite = icons[0];
                    }
                    else
                    {
                        state = STATE.CLAIMED;
                        chestBTN.GetComponent<Image>().sprite = icons[1];
                        TimeSpan interval = currentTime - claimTime;
                        currentSeconds = (float)interval.TotalSeconds*-1;
                    }
                }
                else
                {
                    state = STATE.CLAIMAVAILABLE;
                    chestBTN.GetComponent<Image>().sprite = icons[0];
                }
                break;
            case UnityWebRequest.Result.ConnectionError:
                StartCoroutine(GetCurrentTime());
                break;
            case UnityWebRequest.Result.ProtocolError:
                StartCoroutine(GetCurrentTime());
                break;
            case UnityWebRequest.Result.DataProcessingError:
                StartCoroutine(GetCurrentTime());
                break;
            default:
                break;
        }
    }

    private string ReturnStringFromWorldTimeApi(string data)
    {
        MyCustomDateTimeClass myObject = new MyCustomDateTimeClass();

        myObject = JsonUtility.FromJson<MyCustomDateTimeClass>(data);

        return myObject.datetime;
    }
    private void Claim()
    {
        //state = STATE.CLAIMED;
        //currentSeconds = 7200;
        //claimTime = currentTime;
        //claimTime = claimTime.AddHours(2);
        //chestBTN.GetComponent<Image>().sprite = icons[1];
        //PlayerPrefs.SetString("HourlyReward", claimTime.ToBinary().ToString());
        //dailySpin.SetActive(true);
        switch (state)
        {
            case STATE.CLAIMAVAILABLE:
                switch (SwitchAdsNetwork.instance.switchAds)
                {
                    case SwitchAds.UnityAds:
                        break;
                    case SwitchAds.Ironsource:
                        IronsourceAdsManager.Instance.RequestRewardedAds("HourlyReward");
                        break;
                    case SwitchAds.GoogleAdMob:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case STATE.WAITING:
                break;
            case STATE.CLAIMAVAILABLE:
                currentSeconds = 0;
                timerTeks.text = "CLAIM REWARD!";
                break;
            case STATE.CLAIMED:
                currentSeconds -= Time.deltaTime;

                // Menghitung jam, menit, dan detik
                int hours = Mathf.FloorToInt(currentSeconds / 3600);
                int minutes = Mathf.FloorToInt((currentSeconds % 3600) / 60);
                int seconds = Mathf.FloorToInt(currentSeconds % 60);

                // Format waktu menjadi teks
                string timerText = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);

                timerTeks.text = timerText;
                if (currentSeconds <= 0)
                {
                    state = STATE.CLAIMAVAILABLE;

                }    
                break;
            default:
                break;
        }
    }
}
public enum STATE
{
    WAITING,
    CLAIMAVAILABLE,
    CLAIMED
}