using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Events;

namespace DevTeam
{
    public class AdsManager : MonoBehaviour
    {
        public static AdsManager Instance;

        protected string adUnitId;

        [SerializeField] protected bool isTestDevelopment;

        [SerializeField, TextArea(3,3)] protected string bannerId;
        [SerializeField, TextArea(3, 3)] protected string interstitialId;
        [SerializeField, TextArea(3, 3)] protected string rewardId;


        public UnityEvent OnAdsClick;

        public UnityEvent OnAdsFailed;
        public UnityEvent OnAdsOpen;
        public UnityEvent OnAdsClose;

        public UnityEvent OnGetReward;

        public UnityEvent OnImpressionRecorded;
        public UnityEvent OnAdPaid;

        [SerializeField] private AdPosition AdPositionBanner;
        
        protected BannerView bannerView;
        
        protected List<string> deviceIds = new List<string>();

        #region SINGLETON
        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                //Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }
        #endregion

        protected void Start()
        {
            MobileAds.Initialize(InitStatus => { });

            if(!isTestDevelopment)
            {
                return;
            }

            AddNewDevices("106d83047e564f5e9fdf8403311d70aa");
        }

        public void AddNewDevices(string newDeviceId)
        {
            deviceIds.Add(newDeviceId);

            RequestConfiguration requestConfiguration = new RequestConfiguration
                .Builder()
                .SetTestDeviceIds(deviceIds)
                .build();

            MobileAds.SetRequestConfiguration(requestConfiguration);

            //RequestBanner();
        }

        public void RequestBanner()
        {
            #if UNITY_ANDROID
                        adUnitId = bannerId;
            #elif UNITY_IPHONE
                        adUnitId = "ca-app-pub-3940256099942544/2934735716";
            #else
                        adUnitId = "unexpected_platform";
            #endif

            bannerView = new BannerView(adUnitId, AdSize.IABBanner, AdPositionBanner);

            bannerView.OnBannerAdLoaded += HandleAdOpened;
            bannerView.OnBannerAdLoadFailed += HandleAdFailed;

            AdRequest request = new AdRequest.Builder().Build();

            bannerView.LoadAd(request);
        }

        public void CleanUpBanner()
        {
            bannerView.Destroy();
        }

        public void RequestReward()
        {
            #if UNITY_ANDROID
                        adUnitId = rewardId;
            #elif UNITY_IPHONE
                        adUnitId = "ca-app-pub-3940256099942544/1712485313";
            #else
                        adUnitId = "unexpected_platform";
            #endif

            AdRequest request = new AdRequest.Builder().Build();

            RewardedAd.Load(adUnitId, request, OnRewardCallback);
        }

        private void OnRewardCallback(RewardedAd rewardedAd, LoadAdError errorMsg)
        {
            if (!rewardedAd.CanShowAd())
            {
                Debug.LogWarning("Failed to Show Reward Ad because: " + errorMsg.GetMessage());
                OnAdsFailed.Invoke();
                return;
            }
            rewardedAd.Show(OnGetRewardCallback);

            rewardedAd.OnAdClicked += HandleAdClick;
            rewardedAd.OnAdFullScreenContentClosed += HandleAdClosed;
            rewardedAd.OnAdFullScreenContentFailed += HandleAdFailed;
            rewardedAd.OnAdFullScreenContentOpened += HandleAdOpened;
            rewardedAd.OnAdImpressionRecorded += HandleImpressionRecorded;
            rewardedAd.OnAdPaid += HandleAdPaid;
        }

        private void OnGetRewardCallback(Reward rewardAd)
        {
            OnGetReward.Invoke();
        }

        public void RequestInterstitial()
        {
            #if UNITY_ANDROID
                        adUnitId = interstitialId;
            #elif UNITY_IPHONE
                        adUnitId = "ca-app-pub-3940256099942544/4411468910";
            #else
                        adUnitId = "unexpected_platform";
            #endif

            AdRequest request = new AdRequest.Builder().Build();

            InterstitialAd.Load(adUnitId, request, OnInterstitialCallback);
        }

        private void OnInterstitialCallback(InterstitialAd interstitialAd, LoadAdError errorMsg)
        {
            if (!interstitialAd.CanShowAd())
            {
                Debug.LogWarning("Failed to Show Reward Ad because: " + errorMsg.GetMessage());
                OnAdsFailed.Invoke();
                return;
            }
            interstitialAd.Show();

            interstitialAd.OnAdClicked += HandleAdClick;
            interstitialAd.OnAdFullScreenContentClosed += HandleAdClosed;
            interstitialAd.OnAdFullScreenContentFailed += HandleAdFailed;
            interstitialAd.OnAdFullScreenContentOpened += HandleAdOpened;
            interstitialAd.OnAdImpressionRecorded += HandleImpressionRecorded;
            interstitialAd.OnAdPaid += HandleAdPaid;
        }

        private void HandleAdClick()
        {
            // ads content got clicked
            OnAdsClick.Invoke();
        }
        private void HandleAdOpened()
        {
            // ads content opened
            OnAdsOpen.Invoke();
        }

        private void HandleAdClosed()
        {
            // ads content closed
            OnAdsClose.Invoke();
        }

        private void HandleAdFailed(AdError errorMsg)
        {
            // ads failed to load
            Debug.LogError("failed to show ads because :" + errorMsg.GetMessage());
            OnAdsFailed.Invoke();
        }

        private void HandleImpressionRecorded()
        {
            // get impression from ads
            OnImpressionRecorded.Invoke();
        }

        private void HandleAdPaid(AdValue value)
        {
            Debug.Log("On Ad Paid :" + value.Value.ToString());
            OnAdPaid.Invoke();
        }
    }
}
