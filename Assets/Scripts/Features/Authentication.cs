using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Core;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;

public class Authentication : MonoBehaviour
{
    public static Authentication Instance;

    [SerializeField] private bool isSignedIn;

    private async void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }

        await UnityServices.InitializeAsync();
    }

    private void Start()
    {
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.Authenticate((SignInStatus status) =>
        {
            switch (status)
            {
                case SignInStatus.Success:
                    PlayGamesPlatform.Instance.RequestServerSideAccess(true, OnGetTokenCallbacks);
                    break;
                case SignInStatus.InternalError:
                    break;
                case SignInStatus.Canceled:
                    SignInAnonimously();
                    break;
            }
        });
    }

    private async void OnGetTokenCallbacks(string authCode)
    {
        await SignInWithGooglePlayGameServices(authCode);
    }

    private async void SignInAnonimously()
    {
        await SignInAnonymouslyAsync();
    }

    async Task SignInWithGooglePlayGameServices(string authCode)
    {
        SetupEvents();
        try
        {
            await AuthenticationService.Instance.SignInWithGooglePlayGamesAsync(authCode);

            //GetCredential for Firebase Auth
            //Credential credential = GoogleAuthProvider.GetCredential(authCode, null);

            //link with Firebase
            //LinkUserSignInWithFirebase(credential);
        }
        catch (AuthenticationException ex)
        {
            // Compare error code to AuthenticationErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
        catch (RequestFailedException ex)
        {
            // Compare error code to CommonErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
    }

    //private void LinkUserSignInWithFirebase(Credential credential)
    //{
    //    auth.CurrentUser.LinkWithCredentialAsync(credential).ContinueWith(task => {
    //        if (task.IsCanceled)
    //        {
    //            Debug.LogError("LinkWithCredentialAsync was canceled.");
    //            return;
    //        }
    //        if (task.IsFaulted)
    //        {
    //            Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
    //            return;
    //        }

    //        user = task.Result;
    //        Debug.LogFormat("Credentials successfully linked to Firebase user: {0} ({1})", user.DisplayName, user.UserId);
    //    });
    //}

    async Task SignInAnonymouslyAsync()
    {
        SetupEvents();

        try
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();

            // Shows how to get the playerID
            Debug.Log($"PlayerID: {AuthenticationService.Instance.PlayerId} , signed in : {AuthenticationService.Instance.IsSignedIn}");
        }
        catch (AuthenticationException ex)
        {
            // Compare error code to AuthenticationErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
        catch (RequestFailedException ex)
        {
            // Compare error code to CommonErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
    }

    public bool IsSignedIn()
    {
        return isSignedIn;
    }

    private void SetupEvents()
    {
        AuthenticationService.Instance.SignedIn += OnSignedIn;
        AuthenticationService.Instance.SignInFailed += OnSignedInFailed;
        AuthenticationService.Instance.SignedOut += OnSignedOut;
        AuthenticationService.Instance.Expired += OnSessionExpired;
    }

    private void OnDisable()
    {
        AuthenticationService.Instance.SignedIn -= OnSignedIn;
        AuthenticationService.Instance.SignInFailed -= OnSignedInFailed;
        AuthenticationService.Instance.SignedOut -= OnSignedOut;
        AuthenticationService.Instance.Expired -= OnSessionExpired;
    }

    private void OnSignedIn()
    {
        Debug.Log($"<color=green>Signed In Success! PlayerID: {AuthenticationService.Instance.PlayerId} , Access Token: {AuthenticationService.Instance.AccessToken}</color>");
        isSignedIn = true;
    }

    private void OnSignedInFailed(RequestFailedException message)
    {
        Debug.Log($"<color=green>Player Failed to signed in! cause : {message.Message}</color>");
    }

    private void OnSignedOut()
    {
        Debug.Log($"<color=green>Player Signed Out..</color>");
    }

    private void OnSessionExpired()
    {
        Debug.Log($"<color=green>Player session could not be refreshed and expired.</color>");
    }

    public void SignedOut()
    {
        AuthenticationService.Instance.SignOut();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.H))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
