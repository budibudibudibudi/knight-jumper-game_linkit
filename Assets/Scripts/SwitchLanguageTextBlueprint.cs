using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class SwitchLanguageTextBlueprint
{
    public string namaGameobject;
    public Text tekstKomponen;
    [HideInInspector]public string teksAwal;
    [TextArea(1,5)]
    public string terjemahan;
}
