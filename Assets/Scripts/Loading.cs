﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    public int timerLoad;
    public string toScene;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadGame());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LoadGame(){
        yield return new WaitForSeconds(timerLoad);
        SceneManager.LoadScene(toScene);
    }

    public void SkipStory()
    {
        SceneManager.LoadScene(toScene);
    }
}
