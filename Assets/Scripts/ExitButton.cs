﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitButton : MonoBehaviour
{

    public GameObject closeButton1, closeButton2;

    public GameObject[] ButtonReplyRandom;

    void Start(){
        ButtonReplyRandom[1].GetComponent<Button>().onClick.AddListener(() => {
            UnityAdsManager.Instance.ReqeustAds("KeGame");
            Time.timeScale = 0;
            FindObjectOfType<GameManager>().DestroyPlayer();
            FindObjectOfType<GameManager>().KeGame();
            });
    }

    public void CloseButtonFalse(){
        closeButton1.SetActive(false);
        closeButton2.SetActive(false);
    }

    IEnumerator ShowRandomExitButton(){
        yield return new WaitForSeconds(3f);
        int i = Random.Range(0, ButtonReplyRandom.Length);
        ButtonReplyRandom[i].SetActive(true);
    }

    public void StartRandomExitButton(){
        StartCoroutine(ShowRandomExitButton());
    }
}
