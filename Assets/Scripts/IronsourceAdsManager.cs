using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IronsourceAdsManager : MonoBehaviour
{
    public GameObject loadingPanel, resultRewardedAdspanel, errorPanel;

    public static IronsourceAdsManager Instance;
    public int Coin;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        string appKey = "188f68f3d";
#elif UNITY_IPHONE
        string appKey = "unexpected_platform";
#else
        string appKey = "unexpected_platform";
#endif
        IronSource.Agent.validateIntegration();
        IronSource.Agent.init(appKey);
        errorPanel.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => {
            Time.timeScale = 1;
            errorPanel.SetActive(false);
        });

        StartCoroutine(loadAds());
    }

    private IEnumerator loadAds()
    {
        IronSource.Agent.loadInterstitial();
        IronSource.Agent.loadRewardedVideo();
        yield return new WaitForSeconds(.5f);
        StartCoroutine(loadAds());
    }

    private void OnEnable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent += SdkInitializationCompletedEvent;


        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

        IronSourceEvents.onRewardedVideoAdRewardedDemandOnlyEvent += RewardedVideoAdRewardedDemandOnlyEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedDemandOnlyEvent += RewardedVideoAdShowFailedDemandOnlyEvent;
        IronSourceEvents.onRewardedVideoAdLoadFailedDemandOnlyEvent += RewardedVideoAdLoadFailedDemandOnlyEvent;


        IronSourceInterstitialEvents.onAdLoadFailedEvent += InterstitialOnAdLoadFailed;
        IronSourceInterstitialEvents.onAdShowFailedEvent += InterstitialOnAdShowFailedEvent;
        IronSourceInterstitialEvents.onAdOpenedEvent += IronSourceInterstitialEvents_onAdOpenedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent += IronSourceInterstitialEvents_onAdClosedEvent;

        IronSourceEvents.onInterstitialAdLoadFailedDemandOnlyEvent += InterstitialAdLoadFailedDemandOnlyEvent;
        IronSourceEvents.onInterstitialAdShowFailedDemandOnlyEvent += InterstitialAdShowFailedDemandOnlyEvent;

    }

    private void OnDisable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent -= SdkInitializationCompletedEvent;


        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;

        IronSourceEvents.onRewardedVideoAdRewardedDemandOnlyEvent -= RewardedVideoAdRewardedDemandOnlyEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedDemandOnlyEvent -= RewardedVideoAdShowFailedDemandOnlyEvent;
        IronSourceEvents.onRewardedVideoAdLoadFailedDemandOnlyEvent -= RewardedVideoAdLoadFailedDemandOnlyEvent;


        IronSourceInterstitialEvents.onAdLoadFailedEvent -= InterstitialOnAdLoadFailed;
        IronSourceInterstitialEvents.onAdShowFailedEvent -= InterstitialOnAdShowFailedEvent;
        IronSourceInterstitialEvents.onAdOpenedEvent -= IronSourceInterstitialEvents_onAdOpenedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent -= IronSourceInterstitialEvents_onAdClosedEvent;

        IronSourceEvents.onInterstitialAdLoadFailedDemandOnlyEvent -= InterstitialAdLoadFailedDemandOnlyEvent;
        IronSourceEvents.onInterstitialAdShowFailedDemandOnlyEvent -= InterstitialAdShowFailedDemandOnlyEvent;

    }
    private void SdkInitializationCompletedEvent()
    {
        IronSource.Agent.loadInterstitial();
        IronSource.Agent.loadRewardedVideo();
    }

    #region interstitial event

    private void InterstitialAdShowFailedDemandOnlyEvent(string arg1, IronSourceError arg2)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = arg2.getDescription();
    }

    private void InterstitialAdLoadFailedDemandOnlyEvent(string arg1, IronSourceError arg2)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = arg2.getDescription();
    }

    private void InterstitialOnAdShowFailedEvent(IronSourceError arg1, IronSourceAdInfo arg2)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = arg1.getDescription();
    }

    private void InterstitialOnAdLoadFailed(IronSourceError obj)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = obj.getDescription();
    }

    public void IronSourceInterstitialEvents_onAdClosedEvent(IronSourceAdInfo obj)
    {
        errorPanel.SetActive(true);
        errorPanel.GetComponentInChildren<Text>().text = "Interstitial ads 1";
        Time.timeScale = 1;
    }
    private void IronSourceInterstitialEvents_onAdOpenedEvent(IronSourceAdInfo obj)
    {
        Time.timeScale = 0;
    }


    #endregion
    #region rewarded event

    private void RewardedVideoAdShowFailedDemandOnlyEvent(string arg1, IronSourceError arg2)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = arg2.getDescription();
    }

    private void RewardedVideoAdLoadFailedDemandOnlyEvent(string arg1, IronSourceError arg2)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = arg2.getDescription();
    }

    private void RewardedVideoAdShowFailedEvent(IronSourceError obj)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = obj.getDescription();
    }

    private void RewardedVideoAdRewardedDemandOnlyEvent(string obj)
    {
        //Dictionary<string, string> AchievementEvent = new Dictionary<string, string>();
        //AchievementEvent.Add(AFInAppEvents.ACHIEVEMENT_UNLOCKED, "Ironsource Ads");
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
        adjustAdRevenue.setAdRevenuePlacement(obj);
        adjustAdRevenue.setAdRevenueNetwork("Ironsource");
        Time.timeScale = 1;
        ActivityPoint.Instance.AddOrSubActivityPoint(10);
        if (obj == "Coin")
        {
            FindObjectOfType<GameManager>().AddBonusCoin(Coin);
            resultRewardedAdspanel.SetActive(true);
            ResultRewardedAds.instance.Result(Coin, "Coin");
            FindObjectOfType<GameManager>().PanelRevive.SetActive(false);
            //AchievementEvent.Add(AFInAppEvents.PARAM_1, "Get Bonus Coin");
            adjustAdRevenue.addCallbackParameter($"Watch {obj} ads", "Get Bonus Coin");
        }
        else if (obj == "Revive")
        {
            StartCoroutine(FindObjectOfType<GameManager>().Revive());
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {obj} ads", "Use Revive");
        }
        //AppsFlyerSDK.AppsFlyer.sendEvent("af_IronsourceAds", AchievementEvent);
        Adjust.trackAdRevenue(adjustAdRevenue);
        loadingPanel.SetActive(false);
        IronSource.Agent.loadRewardedVideo();
    }

    private void RewardedVideoAdStartedEvent()
    {
        loadingPanel.SetActive(false);
    }

    private void RewardedVideoAdRewardedEvent(IronSourcePlacement obj)
    {
        //Dictionary<string, string> AchievementEvent = new Dictionary<string, string>();
        //AchievementEvent.Add(AFInAppEvents.ACHIEVEMENT_UNLOCKED, "Ironsource Ads");
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
        adjustAdRevenue.setAdRevenuePlacement(obj.getPlacementName());
        adjustAdRevenue.setAdRevenueNetwork("Ironsource");
        Time.timeScale = 1;
        ActivityPoint.Instance.AddOrSubActivityPoint(10);
        if (obj.getPlacementName() == "Coin")
        {
            FindObjectOfType<GameManager>().AddBonusCoin(Coin);
            resultRewardedAdspanel.SetActive(true);
            ResultRewardedAds.instance.Result(Coin, "Coin");
            FindObjectOfType<GameManager>().PanelRevive.SetActive(false);
            //AchievementEvent.Add(AFInAppEvents.PARAM_1, "Get Bonus Coin");
            adjustAdRevenue.addCallbackParameter($"Watch {obj.getPlacementName()} ads", "Get Bonus Coin");
        }
        else if(obj.getPlacementName() == "Revive")
        {
            StartCoroutine(FindObjectOfType<GameManager>().Revive());
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {obj.getPlacementName()} ads", "Get Bonus Coin");
        }
        //AppsFlyerSDK.AppsFlyer.sendEvent("af_IronsourceAds", AchievementEvent);
        Adjust.trackAdRevenue(adjustAdRevenue);
        loadingPanel.SetActive(false);
        IronSource.Agent.loadRewardedVideo();
    }
    private void RewardedEvent(IronSourcePlacement arg1, IronSourceAdInfo arg2)
    {
        //Dictionary<string, string> AchievementEvent = new Dictionary<string, string>();
        //AchievementEvent.Add(AFInAppEvents.ACHIEVEMENT_UNLOCKED, "Ironsource Ads");
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
        adjustAdRevenue.setAdRevenuePlacement(arg1.getPlacementName());
        adjustAdRevenue.setAdRevenueNetwork(arg2.adNetwork);
        adjustAdRevenue.setAdRevenueUnit(arg2.adUnit);
        adjustAdRevenue.setRevenue(arg2.revenue, "USD");
        Time.timeScale = 1;
        ActivityPoint.Instance.AddOrSubActivityPoint(10);
        if (arg1.getPlacementName() == "Coin")
        {
            FindObjectOfType<GameManager>().AddBonusCoin(Coin);
            resultRewardedAdspanel.SetActive(true);
            ResultRewardedAds.instance.Result(Coin, "Coin");
            FindObjectOfType<GameManager>().PanelRevive.SetActive(false);
            //AchievementEvent.Add(AFInAppEvents.PARAM_1, "Get Bonus Coin");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Get Bonus Coin");
            AdjustEvent adjustEvent = new AdjustEvent("7xccam");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "Revive" )
        {
            StartCoroutine(FindObjectOfType<GameManager>().Revive());
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Get Revived");
            AdjustEvent adjustEvent = new AdjustEvent("tdpz4l");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "Fortune_Wheeler" )
        {
            DailyReward.instance.rewardClaimedPanel.SetActive(true);
            DailyReward.instance.rewardClaimedPanel.transform.GetChild(3).GetComponent<Image>().sprite = DailyReward.instance.rewardsWeekData[DailyReward.instance.today].GetRewardclass().image;
            DailyReward.instance.pemisah = DailyReward.instance.rewardsWeekData[DailyReward.instance.today].GetRewardclass().nama.Split(" ");
            DailyReward.instance.rewardClaimedPanel.transform.GetChild(4).GetComponent<Text>().text = "You Got : " + DailyReward.instance.rewardsWeekData[DailyReward.instance.today].GetRewardclass().amount + " " + DailyReward.instance.pemisah[0];
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Get Daily Reward");
            AdjustEvent adjustEvent = new AdjustEvent("sw151l");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "GameOver")
        {
            GameManager gm = FindObjectOfType<GameManager>();
            if (GameManager.Score >= gm.MunculPanelGameOver)
            {
                gm.GameOverPanel.SetActive(true);
                if (gm.deathCount == 0)
                {
                    gm.GameOverPanel.GetComponent<ExitButton>().StartRandomExitButton();
                    gm.LosePanel.SetActive(true);
                    gm.deathCount++;
                }
                else if (gm.deathCount > 0)
                {
                    gm.GameOverPanel.GetComponent<ExitButton>().StartRandomExitButton();
                    gm.retryPanel.SetActive(true);
                    gm.deathCount = 0;
                }
            }

            else if (GameManager.Score < gm.MunculPanelGameOver)
            {
                gm.DestroyPlayer();
                gm.KeGame();
            }
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Game Over");
            AdjustEvent adjustEvent = new AdjustEvent("l6qi34");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "KeGame")
        {
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Back to menu");
            AdjustEvent adjustEvent = new AdjustEvent("kof65h");
            Adjust.trackEvent(adjustEvent);
            FindObjectOfType<GameManager>().DestroyPlayer();
            SceneManager.LoadScene("Game");
        }
        else if (arg1.getPlacementName() == "FinishGame")
        {
            CinematicEndGame.Instance.EndGame(FindObjectOfType<GameManager>().currentScene);
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Finih the level");
            AdjustEvent adjustEvent = new AdjustEvent("wfcawp");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "RestartGame")
        {
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Restart game");
            AdjustEvent adjustEvent = new AdjustEvent("jr9hwy");
            Adjust.trackEvent(adjustEvent);
            ActivityPoint.Instance.AddOrSubActivityPoint(2);
            FindObjectOfType<GameManager>().PlayerAnim.GetComponent<Player>().SetToIdle();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else if (arg1.getPlacementName() == "HourlyReward")
        {
            HourlyReward hr = FindObjectOfType<HourlyReward>();
            hr.state = STATE.CLAIMED;
            hr.chestBTN.GetComponent<Image>().sprite = hr.icons[1];
            hr.currentSeconds = 7200;
            hr.claimTime = hr.currentTime;
            hr.claimTime = hr.claimTime.AddHours(2);
            PlayerPrefs.SetString("HourlyReward", hr.claimTime.ToBinary().ToString());
            hr.dailySpin.SetActive(true);
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Hourly Reward");
            AdjustEvent adjustEvent = new AdjustEvent("v78363");
            Adjust.trackEvent(adjustEvent);
        }
        else if (arg1.getPlacementName() == "CrackEgg")
        {
            FindObjectOfType<CrackEgg>().showedAds = true;
            //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
            adjustAdRevenue.addCallbackParameter($"Watch {arg1.getPlacementName()} ads", "Crack Egg");
            AdjustEvent adjustEvent = new AdjustEvent("xkqr4e");
            Adjust.trackEvent(adjustEvent);
        }
        //AppsFlyerSDK.AppsFlyer.sendEvent("af_IronsourceAds", AchievementEvent);
        Adjust.trackAdRevenue(adjustAdRevenue);
        loadingPanel.SetActive(false);
        IronSource.Agent.loadRewardedVideo();
    }
    #endregion
    #region Request Ads

    public void RequestInterstitialAds(string? placement)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //errorPanel.SetActive(true);
            //errorPanel.GetComponentInChildren<Text>().text = "Please connect to internet";
            Time.timeScale = 0;
        }
        loadingPanel.SetActive(true);
        IronSource.Agent.showInterstitial(placement);
    }
    public void RequestRewardedAds(string placement)
    {
#if !UNITY_EDITOR
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //errorPanel.SetActive(true);
            //errorPanel.GetComponentInChildren<Text>().text = "Please connect to internet";
            Time.timeScale = 0;
        }
        loadingPanel.SetActive(true);
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo(placement);
        }
        else
        {
            RequestRewardedAds(placement);
        }

#endif
    }
    #endregion
}
