﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour
{
    public int GuideInt;
    public GameObject GuideObj, ReadyObj;

    public string GuideID;

    public GameManager GMScript;
    public int ModeGamePlay;
    // Start is called before the first frame update
    void Start()
    {
        ModeGamePlay = PlayerPrefs.GetInt("ModeGamePlay");

        if (ModeGamePlay == 1){
            GuideObj.SetActive(false);
        }
        if (ModeGamePlay == 0){
            GuideInt = PlayerPrefs.GetInt(GuideID);
            if (GuideInt == 0){
                GuideObj.SetActive(true);
            }
            if (GuideInt == 1){
                GuideObj.SetActive(false);
                ReadyObj.SetActive(true);
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GuideDone(){
        GuideInt = 1;
        PlayerPrefs.SetInt(GuideID, GuideInt);
    }
}
