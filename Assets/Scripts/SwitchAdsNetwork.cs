using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchAdsNetwork : MonoBehaviour
{
    public static SwitchAdsNetwork instance;
    public SwitchAds switchAds;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        switch (switchAds)
        {
            case SwitchAds.UnityAds:
                IronsourceAdsManager.Instance.enabled = false;
                break;
            case SwitchAds.Ironsource:
                UnityAdsManager.Instance.enabled = false;
                break;
            case SwitchAds.GoogleAdMob:
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
public enum SwitchAds
{
    UnityAds,
    Ironsource,
    GoogleAdMob
}
