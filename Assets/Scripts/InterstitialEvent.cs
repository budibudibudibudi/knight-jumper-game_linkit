using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterstitialEvent : MonoBehaviour
{
    public static InterstitialEvent Instance;
    public GameObject errorPanel;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public void RequestInterstitialAds(string? placement)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //errorPanel.SetActive(true);
            //errorPanel.GetComponentInChildren<Text>().text = "Please connect to internet";
            Time.timeScale = 0;
        }
        IronSource.Agent.showInterstitial(placement);  
    }
    private void OnEnable()
    {
        IronSourceInterstitialEvents.onAdClosedEvent += IronSourceInterstitialEvents_onAdClosedEvent;
    }

    private void OnDisable()
    {

        IronSourceInterstitialEvents.onAdClosedEvent -= IronSourceInterstitialEvents_onAdClosedEvent;
    }

    private void IronSourceInterstitialEvents_onAdClosedEvent(IronSourceAdInfo obj)
    {
        errorPanel.SetActive(true);
        errorPanel.GetComponentInChildren<Text>().text = "Interstitial ads 2";
    }
}
