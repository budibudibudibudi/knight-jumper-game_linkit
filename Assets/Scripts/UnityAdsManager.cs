﻿using System;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class UnityAdsManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    public string GAME_ID = "5224621"; //replace with your gameID from dashboard. note: will be different for each platform.

    private const string BANNER_PLACEMENT = "Banner_Android";

    [SerializeField] private BannerPosition bannerPosition = BannerPosition.BOTTOM_CENTER;
    [SerializeField] private bool testMode = false;
    private bool showBanner = false;

    public delegate void DebugEvent(string msg);
    public static event DebugEvent OnDebugLog;
    public GameObject loadingPanel, resultRewardedAdspanel,errorPanel;

    public static UnityAdsManager Instance;
    public int Coin;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        Initialize();
        errorPanel.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(()=> {
            Time.timeScale = 1;
            errorPanel.SetActive(false); 
        });
    }
    public void Initialize()
    {
        if (Advertisement.isSupported)
        {
            DebugLog(Application.platform + " supported by Advertisement");
        }
        Advertisement.Initialize(GAME_ID, testMode, this);
    }

    public void ToggleBanner() 
    {
        showBanner = !showBanner;

        if (showBanner)
        {
            Advertisement.Banner.SetPosition(bannerPosition);
            Advertisement.Banner.Show(BANNER_PLACEMENT);
        }
        else
        {
            Advertisement.Banner.Hide(false);
        }
    }

    public void ReqeustAds(string placementName)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //errorPanel.SetActive(true);
            //errorPanel.GetComponentInChildren<Text>().text = "Please connect to internet";
            Time.timeScale = 0;
        }
        loadingPanel.SetActive(true);
        Advertisement.Load(placementName, this);
    }
    #region Interface Implementations
    public void OnInitializationComplete()
    {

    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        loadingPanel.SetActive(false);
        //errorPanel.SetActive(true);
        //errorPanel.GetComponentInChildren<Text>().text = message;
        Initialize();
    }

    public void OnUnityAdsAdLoaded(string placementId)
    {
        Advertisement.Show(placementId, this);
        DebugLog($"Load Success: {placementId}");
    }
    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
    {
        loadingPanel.SetActive(false);
    }

    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
    {
        loadingPanel.SetActive(false);
    }

    public void OnUnityAdsShowStart(string placementId)
    {
        loadingPanel.SetActive(false);
    }

    public void OnUnityAdsShowClick(string placementId)
    {
        DebugLog($"OnUnityAdsShowClick: {placementId}");
    }
    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
    {
        //Dictionary<string, string> AchievementEvent = new Dictionary<string, string>();
        //AchievementEvent.Add(AFInAppEvents.ACHIEVEMENT_UNLOCKED, "Unity Ads");
        switch (showCompletionState)
        {
            case UnityAdsShowCompletionState.COMPLETED:
                Time.timeScale = 1;
                ActivityPoint.Instance.AddOrSubActivityPoint(10);
                if (placementId == "Coin")
                {
                    FindObjectOfType<GameManager>().AddBonusCoin(Coin);
                    resultRewardedAdspanel.SetActive(true);
                    ResultRewardedAds.instance.Result(Coin,"Coin");
                    FindObjectOfType<GameManager>().PanelRevive.SetActive(false);
                    //AchievementEvent.Add(AFInAppEvents.PARAM_1, "Get Bonus Coin");
                }
                if (placementId == "Revive")
                {
                    StartCoroutine(FindObjectOfType<GameManager>().Revive());
                    //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
                }
                loadingPanel.SetActive(false);
                break;
            case UnityAdsShowCompletionState.UNKNOWN:
                Time.timeScale = 1;
                if (placementId == "Coin")
                {
                    FindObjectOfType<GameManager>().AddBonusCoin(Coin);
                    resultRewardedAdspanel.SetActive(true);
                    ResultRewardedAds.instance.Result(Coin,"Coin");
                    FindObjectOfType<GameManager>().PanelRevive.SetActive(false);
                    //AchievementEvent.Add(AFInAppEvents.PARAM_1, "Get Bonus Coin");
                }
                if (placementId == "Revive")
                {
                    StartCoroutine(FindObjectOfType<GameManager>().Revive());
                    //AchievementEvent.Add(AFInAppEvents.PARAM_2, "Use Revive");
                }
                loadingPanel.SetActive(false);
                break;
            case UnityAdsShowCompletionState.SKIPPED:
                Time.timeScale = 1;
                loadingPanel.SetActive(false);
                break;
        }
        //AppsFlyerSDK.AppsFlyer.sendEvent("af_UnityAds", AchievementEvent);
    }
    #endregion

    //wrapper around debug.log to allow broadcasting log strings to the UI
    void DebugLog(string msg)
    {
        OnDebugLog?.Invoke(msg);
        Debug.Log(msg);
    }
}
